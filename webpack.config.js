const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const fs = require('fs');

const vars = {
  css: {
    title: 'CR DOC CSS',
    viewName: 'CSS',
    color: 'blue',
  },
  html: {
    title: 'CR DOC HTML',
    viewName: 'HTML',
    color: 'pink',
  },
  index: {
    title: 'CR DOC',
    viewName: '',
    color: '',
  },
  info: {
    title: 'CR DOC INFO',
    viewName: 'Info',
    color: 'orange',
  },
  js: {
    title: 'CR DOC JS',
    viewName: 'JavaScript',
    color: 'purple',
  },
  uvod: {
    title: 'CR DOC ÚVOD',
    viewName: 'Úvod',
    color: 'green',
  },
};

function generateHtmlPlugins (templateDir) {
  const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir));

  return templateFiles.map(item => {
    const parts = item.split('.');
    const name = parts[0];
    const extension = parts[1];

    return new HtmlWebpackPlugin({
      filename: `${name}.html`,
      template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`),
      ...vars[name]
    });
  });
}

const htmlPlugins = generateHtmlPlugins('./src/views');

module.exports = {
  mode: 'development',
  entry: './src/index.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  },

  module: {
    rules: [
      {
        test: /\.s[ac]ss$/i,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader'
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader'
        ]
      },
      {
        test: /\.hbs$/,
        loader: 'handlebars-loader'
      },
    ],
  },
  devtool: 'inline-source-map',
    devServer: {
      contentBase: './dist'
    },
  plugins: [
    new CleanWebpackPlugin(),
  ].concat(htmlPlugins),
};