import style from './scss/main.scss';
import Typed from 'typed.js'
import Prism from 'prismjs';

var options = {
    stringsElement: '#typed-strings',
    typeSpeed: 80,
    showCursor: false,
    backDelay: 2000,
    loop: true
  };
  
  var typed = new Typed('.hello_world', options);